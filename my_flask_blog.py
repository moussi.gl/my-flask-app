from flask import Flask, render_template
app = Flask(__name__)

posts = [{
    'author': 'Aymne MOUSSI',
    'title': 'Post 1',
    'content': 'post content 1',
    'date_posted': 'April 21, 2018'
},
    {
        'author': 'Achref MOUSSI',
        'title': 'Post 2',
        'content': 'post content 2',
        'date_posted': 'April 22, 2018'
    }
]

@app.route('/')
@app.route('/home')
def home():
    return render_template('home.html', posts=posts)


@app.route('/about')
def about():
    return render_template('about.html', title="About")


if __name__ == '__main__':
    app.run(debug=True)
